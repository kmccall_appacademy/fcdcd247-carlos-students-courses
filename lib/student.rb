class Student
  attr_accessor :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  def name
    first_name + " " + last_name
  end

  def enroll(course)
    unless @courses.include?(course)
      raise if @courses.reduce(false) { |carry, enrolled_course| carry || enrolled_course.conflicts_with?(course) }
      course.students << self
      @courses << course
    end
  end

  def course_load
    load_hash = {}
    @courses.each do |course|
      unless load_hash.key?(course.department)
        load_hash[course.department] = course.credits
      else
        load_hash[course.department] += course.credits
      end
    end
    load_hash
  end
end
